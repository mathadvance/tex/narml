\ProvidesClass{mat}

\LoadClass{article}

\PassOptionsToPackage{dvipsnames}{xcolor}
\RequirePackage{xcolor} % has to be before tikz

\RequirePackage{amsmath,amssymb}
\RequirePackage{asymptote,tikz,tikz-3dplot}

\pagenumbering{gobble}

% Set dimensions via geometry.
\RequirePackage[letterpaper,top=2cm,bottom=4cm,left=3cm,right=3cm,includeheadfoot]{geometry}

%%%%%%%%%
%LENGTHS%
%%%%%%%%%

\setlength{\parindent}{0pt}

%%%%%%%
%FONTS%
%%%%%%%

\DeclareRobustCommand{\alegreyafont}{\fontfamily{Alegreya-LF}\selectfont} % defines alegreya
\DeclareRobustCommand{\parttitlefont}{\alegreyafont}

\RequirePackage{newpxtext,newpxmath}

%%%%%%%%%%%%%%%%%%
%PROB/SOL DISPLAY%
%%%%%%%%%%%%%%%%%%

\newenvironment{content}{\vskip -2mm\setlength{\parskip}{1ex plus 0.5ex minus 0.2ex}}{}

\newcommand{\ansbold}[1]{
	\ifmmode
		{\sectioncolor\mathbf{#1}}
	\else
		\ClassError{ma}{The \protect\ansbold command must be used in math mode.}
	\fi
}

%%%%%%%%
%COLORS%
%%%%%%%%

\definecolor{darkmidnightblue}{HTML}{003366}

\newcommand{\sectioncolor}{\color{darkmidnightblue}}

%%%%%%
%LOGO%
%%%%%%

\newcommand{\logo}[1]{ %
    \tdplotsetmaincoords{60}{100}
    \pgfmathsetmacro\logoscale{#1}
    \begin{tikzpicture}[tdplot_main_coords,scale=\logoscale,line join=round]
        \pgfmathsetmacro\a{-2}
        \pgfmathsetmacro{\phi}{\a*(1+sqrt(5))/2}
        \pgfmathsetmacro{\psi}{\a*(sqrt(5)-1)/2}
        \path 
        coordinate(A) at (0,-\phi,\psi)
        coordinate(B) at (0,-\phi,-\psi)
        coordinate(C) at (\a,-\a,-\a)
        coordinate(D) at (\phi,-\psi,0)
        coordinate(E) at (\a,-\a,\a)
        coordinate(F) at (-\a,-\a,-\a)
        coordinate(G) at (-\phi,-\psi,0)
        coordinate(H) at (-\a,-\a,\a)
        coordinate(I) at (-\psi,0,-\phi)
        coordinate(J) at (\psi,0,-\phi)
        coordinate(K) at (\a,\a,-\a)
        coordinate(L) at (\phi,\psi,0)
        coordinate(M) at (\a,\a,\a)
        coordinate(N) at (\psi,0,\phi)
        coordinate(O) at (-\psi,0,\phi)
        coordinate(P) at (-\phi,\psi,0)
        coordinate(Q) at (-\a,\a,-\a)
        coordinate(R) at (0,\phi,-\psi)
        coordinate(S) at (0,\phi,\psi)
        coordinate(T) at (-\a,\a,\a); 
        \fill [RoyalBlue,opacity=0.4] 
        (D) -- (L) -- (M) -- (N) -- (E) -- cycle;
        \fill [Goldenrod, opacity=0.4]
        (A) -- (B) -- (C) -- (D) -- (E) -- cycle;
        \fill [ForestGreen, opacity=0.4]
        (C) -- (J) -- (K) -- (L) -- (D) -- cycle;
        \fill [RubineRed, opacity=0.4]
        (E) -- (N) -- (O) -- (H) -- (A) -- cycle;
        \fill [Orchid, opacity=0.4]
        (K) -- (R) -- (S) -- (M) -- (L) -- cycle;
        \fill [orange, opacity=0.4]
        (O) -- (T) -- (S) -- (M) -- (N) -- cycle;
        \draw[thick]
        (A) -- (B) -- (C) -- (D) -- (E) -- cycle
        (D) -- (L) -- (M) -- (N) -- (E)
        (C) -- (J) -- (K) -- (L) -- (D)
        (E) -- (N) -- (O) -- (H) -- (A)
        (K) -- (R) -- (S) -- (M) -- (N)
        (O) -- (T) -- (S);
    \end{tikzpicture}
}

%%%%%%%%%
%HEADERS%
%%%%%%%%%

\RequirePackage{graphicx}

\RequirePackage{fancyhdr}
\pagestyle{fancyplain}
\renewcommand{\headrulewidth}{0pt}
\fancyhead{}

\fancyfoot[L]{
	\hspace{-20pt}\raisebox{-63pt}[0pt][0pt]{
		\logo{0.27}}
	\raisebox{-56pt}[0pt][0pt]{%
		\begin{tikzpicture}
			\draw (0,0) node {{\alegreyafont\fontsize{40}{50}\selectfont MAC}};
		\end{tikzpicture}%
	}
}

\renewcommand\maketitle{
	\thispagestyle{empty}
	\begin{center}
		{\Huge\parttitlefont\@title}
		\vskip\baselineskip
		\logo{0.55}
		\vskip\baselineskip
	\end{center}
}

\renewcommand{\part}[1]{
	\newpage
	\begin{center}
		{\Huge\parttitlefont#1}
		\vskip\baselineskip
	\end{center}
}

\renewcommand{\section}[1]{
	\vskip 6mm
		{\LARGE\sectioncolor\bfseries\sffamily #1}
	\vskip 2mm
}

\renewcommand{\subsection}[1]{
	\vskip 6mm
		{\large\sectioncolor\bfseries\sffamily #1}
	\vskip 2mm
}

%%%%%%%
%ITEMS%
%%%%%%%

\RequirePackage{pifont}
\renewcommand{\labelitemi}{\ding{70}}

\renewcommand\emph[1]{\textbf{#1}}
